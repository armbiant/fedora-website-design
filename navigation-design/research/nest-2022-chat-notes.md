# Feedback from Nest 2022 Test Session

## User Testing Notes

### General

- Some stable links across contexts could be helpful (pages that all sites relate to)

### Discussions (Message Board)

- Users found that term "Discussions" was better than "Hyperkitty"
- Searched for Discussions under Community
- 2 clicks
- Easy to find (hyperkitty was confusing)

### KDE

- users went to "editions, then to KDE"
- This was easy to find

### Magazine

- term "news" was a bit confusing
- Magazine is for End Users
- Community Blog is more contributor focused
- some felt that the magazine links should grouped under community

### Docs

- This had some confusion
- Some users went to Support and then to Docs Home
- Others went to Editions first and then support and then docs
  - still got there but 3 steps
- Split was between people who:
  - Thought docs for specific editions (topic -> doc)
  - Thought docs in terms of support (need help -> doc)

---

## Takeaways

- Some items need to be moved to global level
- Terminology may need some changing
- Generally positive feedback towards this direction
